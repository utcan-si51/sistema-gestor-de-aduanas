package com.example.david.sistemasaduana2;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AduanaPrincipal extends AppCompatActivity {
    private Button boton;
    private FloatingActionButton anadir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aduana_principal);

        boton=(Button) findViewById(R.id.anadeAduana1);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Aduana_anade2();
            }
        });

        anadir=(FloatingActionButton) findViewById(R.id.anadeAduana12);
        anadir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Aduana_anade2();
            }
        });
    }

    public void Aduana_anade2() {
        Intent i = new Intent(this, Aduana_anade.class);
        startActivity(i);
    }
}
