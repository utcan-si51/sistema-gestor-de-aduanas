package com.example.david.sistemasaduana2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button boton;
    private EditText usuario,password;
    String user=null;
    String pass=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        boton=(Button) findViewById(R.id.bt1);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuAdmin();
            }
        });

    }

    public void MenuAdmin() {
        usuario=(EditText) findViewById(R.id.et1);
        password=(EditText) findViewById(R.id.et2);
        user=usuario.getText().toString();
        pass=password.getText().toString();
        if(user.matches("") || pass.matches("")){
            Toast errorn =Toast.makeText(getApplicationContext(),"Campo/os vacios Men", Toast.LENGTH_SHORT);
            errorn.show();
        }else {
            if (user.equals("David") && pass.equals("123")) {
                Intent i = new Intent(this, MenuAdminActivity.class);
                startActivity(i);
            } else {
                Toast erroru =Toast.makeText(getApplicationContext(),"Usuario o Contraseña mal Men", Toast.LENGTH_SHORT);
                erroru.show();
            }
        }
    }
}
