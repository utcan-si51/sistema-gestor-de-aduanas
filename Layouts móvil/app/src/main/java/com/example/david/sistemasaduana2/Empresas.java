package com.example.david.sistemasaduana2;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Empresas extends AppCompatActivity {
    private Button boton;
    private FloatingActionButton anadir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresas);

        boton=(Button) findViewById(R.id.anadeEmpresas);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EmpresaA();
            }
        });

        anadir=(FloatingActionButton) findViewById(R.id.anadeColaborador);
        anadir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EmpresaA();
            }
        });
    }



public void  EmpresaA() {
            Intent i = new Intent(this, EmpresaGE.class);
        startActivity(i);
        }


}
//anadeEmpresas