package com.example.david.sistemasaduana2;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ImportePrincipal extends AppCompatActivity {
    private Button boton;
    private FloatingActionButton anadir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_importe_principal);

        boton=(Button) findViewById(R.id.consultar);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Aduana_anade();
            }
        });

        anadir=(FloatingActionButton) findViewById(R.id.anadeAduana1);
        anadir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Aduana_anade();
            }
        });

    }

    public void Aduana_anade() {
        Intent i = new Intent(this, ImporteGE.class);
        startActivity(i);
    }

}
