package com.example.david.pruebamysql;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Actualizar extends AppCompatActivity implements View.OnClickListener {
    private EditText editTextN,editTextP,editTextC;
    private Button guardar,regresar;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar);

        editTextN=(EditText) findViewById(R.id.editTextN);
        editTextP=(EditText) findViewById(R.id.editTextP);
        editTextC=(EditText) findViewById(R.id.editTextC);

        guardar=(Button) findViewById(R.id.btnenviar);
        regresar=(Button) findViewById(R.id.btnregresar);

        progressDialog=new ProgressDialog(this);

        guardar.setOnClickListener(this);
        regresar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view==guardar){
           modificar();
        }else if(view==regresar){
            cerrar();
        }
    }

    //Proceso de eliminar
    private void modificar(){
        final  String correo= editTextC.getText().toString().trim();
        final  String nombre= editTextN.getText().toString().trim();
        final  String contra= editTextP.getText().toString().trim();
        progressDialog.setMessage("Modificando usuario...");
        progressDialog.show();
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                Constants.URL_Modificar,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            Toast.makeText(getApplicationContext(),jsonObject.getString("mensaje"),Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hide();
                Toast.makeText(getApplication(), error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("nombre",nombre);
                params.put("contrasena",contra);
                params.put("correo",correo);
                return  params;
            }
        };
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    public void cerrar(){
        finish();
    }

}
