package com.example.david.pruebamysql;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener{
private EditText editTextN,editTextP,editTextC;
private Button guardar,eliminar,actualizar,mostrar;
private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextN=(EditText) findViewById(R.id.editTextN);
        editTextP=(EditText) findViewById(R.id.editTextP);
        editTextC=(EditText) findViewById(R.id.editTextC);

        guardar=(Button) findViewById(R.id.btnenviar);
        actualizar=(Button) findViewById(R.id.btnupdate);
        eliminar=(Button) findViewById(R.id.btndelete);
        mostrar=(Button) findViewById(R.id.btnmostrar);

        progressDialog=new ProgressDialog(this);

        guardar.setOnClickListener(this);
        actualizar.setOnClickListener(this);
        eliminar.setOnClickListener(this);
        mostrar.setOnClickListener(this);
    }

    private void registrar(){
       final  String correo= editTextC.getText().toString().trim();
       final  String nombre= editTextN.getText().toString().trim();
       final  String contra= editTextP.getText().toString().trim();
       progressDialog.setMessage("Registrando usuario...");
       progressDialog.show();
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                Constants.URL_Guardar,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            Toast.makeText(getApplicationContext(),jsonObject.getString("mensaje"),Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hide();
                Toast.makeText(getApplication(), error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
               Map<String,String> params=new HashMap<>();
               params.put("nombre",nombre);
               params.put("contrasena",contra);
               params.put("correo",correo);
               return  params;
            }
        };
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);



    }

    @Override
    public void onClick(View view) {
        if(view==guardar){
            registrar();
        }else if(view==actualizar){
            Intent intento= new Intent(this, Actualizar.class);
            startActivity(intento);
           //Toast.makeText(getApplication(),"Actualizar",Toast.LENGTH_LONG).show();
        }else if(view==eliminar){
            Intent intento2= new Intent(this, Eliminar.class);
            startActivity(intento2);
            //Toast.makeText(getApplication(),"Eliminar",Toast.LENGTH_LONG).show();
        }else if(view==mostrar){
            Intent intento3= new Intent(this, Mostrar.class);
            startActivity(intento3);
        }
    }
}
