package com.example.david.pruebamysql;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Mostrar extends AppCompatActivity implements View.OnClickListener {
private EditText editTextID,editTextC,editTexP,editTextN;
private Button regresar,mostrar;
private ProgressDialog progressDialog;
@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar);
        editTexP=(EditText) findViewById(R.id.editTextP);
        editTextID=(EditText) findViewById(R.id.editTextId);
        editTextC=(EditText) findViewById(R.id.editTextC);
        editTextN=(EditText) findViewById(R.id.editTextN);

    mostrar=(Button) findViewById(R.id.btnenviar);
    regresar=(Button) findViewById(R.id.btnregresar);

    progressDialog=new ProgressDialog(this);

    mostrar.setOnClickListener(this);
    regresar.setOnClickListener(this);

    }

    public void Mostar(){
        final  String correo= editTextC.getText().toString().trim();

        progressDialog.setMessage("Buscando usuario...");
        progressDialog.show();
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                Constants.URL_Mostrar,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            Toast.makeText(getApplication(),"Encontrado",Toast.LENGTH_LONG).show();
                            String id=jsonObject.getString("id");
                            String contrasena=jsonObject.getString("contrasena");
                            String nombre=jsonObject.getString("nombre");
                            editTextID.setText(id);
                            editTexP.setText(contrasena);
                            editTextN.setText(nombre);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hide();
                Toast.makeText(getApplication(), error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("correo",correo);
                return  params;
            }
        };
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View view) {
        if(view==mostrar){
            Mostar();
        }else if(view==regresar){
            cerrar();
        }
    }

    public void cerrar(){
        finish();
    }
}
