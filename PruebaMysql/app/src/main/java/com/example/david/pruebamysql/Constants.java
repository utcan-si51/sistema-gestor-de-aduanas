package com.example.david.pruebamysql;

/**
 * Created by David on 21/03/2018.
 */

public class Constants {

   // private static final String Root_URL="http://10.0.0.9/Prueba/servicios/";
    private static final String Root_URL="http://jvaldez.esy.es/Registro_users/servicios/";
    public static final String URL_Guardar=Root_URL+"agregar.php";
    public static final String URL_Eliminar=Root_URL+"eliminar.php";
    public static final String URL_Modificar=Root_URL+"modificar.php";
    public static final String URL_Mostrar=Root_URL+"mostrar.php";
}
