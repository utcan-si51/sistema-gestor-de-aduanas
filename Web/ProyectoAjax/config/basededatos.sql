-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.21-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para vovo1
CREATE DATABASE IF NOT EXISTS `vovo1` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `vovo1`;

-- Volcando estructura para tabla vovo1.aduanas
CREATE TABLE IF NOT EXISTS `aduanas` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) NOT NULL,
  `Direccion` varchar(50) NOT NULL,
  `Estado` varchar(50) NOT NULL,
  `Ciudad` varchar(50) NOT NULL,
  `Pais` varchar(50) NOT NULL,
  `Telefono` varchar(25) NOT NULL,
  `Correo` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla vovo1.aduanas: ~21 rows (aproximadamente)
DELETE FROM `aduanas`;
/*!40000 ALTER TABLE `aduanas` DISABLE KEYS */;
INSERT INTO `aduanas` (`ID`, `Nombre`, `Direccion`, `Estado`, `Ciudad`, `Pais`, `Telefono`, `Correo`) VALUES
	(2, 'saza', 'ZAZA', 'j', 'j', 'j', '9', 'gg@gmail.com'),
	(4, 'Prueba', 'Prueba', 'Prueba', 'Prueba', 'Prueba', '77', 'gg@gmail.com'),
	(5, 'prueba2', 'prueba2', 'prueba2', 'prueba2', 'prueba2', '666', 'ggg@gmail.com'),
	(6, 'Salas', 'prueba2', 'prueba2', 'prueba2', 'prueba2', '777', 'gg@gmail.com'),
	(7, 'La prueba final', 'La prueba final', 'La prueba final', 'La prueba final', 'La prueba final', '7889', 'gg@gmail.com'),
	(9, 'Prueba Final', 'Prueba Final', 'Prueba Final', 'Prueba Final', 'Prueba Final', '666', 'gg@gmail.com'),
	(10, 'Prueba Final2', 'Prueba Final2', 'Prueba Final2', 'Prueba Final2', 'Prueba Final2', '777', 'dasa111.1998@gmail.com'),
	(12, 'Pedro', 'p', 'p', 'p', 'p', '56778', 'felipe@gmail.com'),
	(13, 'Dabid', 'r', 'r', 'r', 'r', '6', 'diego@gmail.com'),
	(14, 'p', 'p', 'p', 'p', 'p', '9', 'lester@gmail.com'),
	(17, 'f', 'k', 'k', 'k', 'k', '0', 'diego@gmail.com'),
	(18, 'manuel', 'm', 'm', 'm', 'm', '9', 'lester@gmail.com'),
	(19, 'p', 'p', 'p', 'p', 'p', '9', 'lester@gmail.com'),
	(22, 'f', 'f', 'f', 'f', 'f', '4', 'gg@gmail.com'),
	(23, 'y', 'y', 'y', 'y', 'y', '8', 'h@gmail.com'),
	(24, 'j', 'j', 'j', 'j', 'j', '89', 'dasa111.1998@gmail.com'),
	(25, 'm', 'm', 'm', 'm', 'm', '98', 'juan@gmail.com'),
	(26, 'Salss', 'pu', 'pjp', 'hhkbk', 'kjjhhj', '89', 'dasa.1998@gmail.com'),
	(27, 'uuu', 'yyy', 'ttt', 'rr', 'rr', '66', 'dasa111.1998@gmail.com'),
	(28, 'y', 'y', 'y', 'y', 'y', '66', 'dasa111.1998@gmail.com');
/*!40000 ALTER TABLE `aduanas` ENABLE KEYS */;

-- Volcando estructura para tabla vovo1.agenciaduanal
CREATE TABLE IF NOT EXISTS `agenciaduanal` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) NOT NULL,
  `RepreRFC` varchar(50) NOT NULL,
  `RazonSocial` varchar(50) NOT NULL,
  `Ciudad` varchar(50) NOT NULL,
  `Estado` varchar(50) NOT NULL,
  `Pais` varchar(50) NOT NULL,
  `Telefono` varchar(50) NOT NULL,
  `Celular` varchar(50) NOT NULL,
  `Correo` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Nombre` (`Nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla vovo1.agenciaduanal: ~0 rows (aproximadamente)
DELETE FROM `agenciaduanal`;
/*!40000 ALTER TABLE `agenciaduanal` DISABLE KEYS */;
/*!40000 ALTER TABLE `agenciaduanal` ENABLE KEYS */;

-- Volcando estructura para tabla vovo1.agenteaduanal
CREATE TABLE IF NOT EXISTS `agenteaduanal` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NombreAgente` varchar(60) NOT NULL,
  `Telefono` varchar(20) NOT NULL,
  `Celular` varchar(20) NOT NULL,
  `Correo` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla vovo1.agenteaduanal: ~84 rows (aproximadamente)
DELETE FROM `agenteaduanal`;
/*!40000 ALTER TABLE `agenteaduanal` DISABLE KEYS */;
INSERT INTO `agenteaduanal` (`ID`, `NombreAgente`, `Telefono`, `Celular`, `Correo`) VALUES
	(1, 'google', '88', '76', 'da@gmail.com'),
	(7, '', '', '', ''),
	(8, 'Hugo', '88', '88', 'da@gmail.com'),
	(9, 'ii', '66', '66', 'da@gmail.com'),
	(17, 'Zalasz', '88', '999', 'j@gmail.com'),
	(18, 'david', '123', '123', 'g@gmail.com'),
	(21, 'Furro', '8990', '890', 'da@gmail.com'),
	(22, 'tt', '44', '44', 'da@gmail.com'),
	(24, 'rr', '44', '44', 'da@gmail.com'),
	(25, 'tt', '123', '123', 'da@gmail.com'),
	(26, 'o', '77', '22', 'da@gmail.com'),
	(27, 'ddd', '666', '666', 'da@gmail.com'),
	(28, 'yyy', '88', '888', 'da@gmail.com'),
	(29, 'jj', '99', '99', 'da@gmail.com'),
	(30, 'Jose', '77', '88', 'da@gmail.com'),
	(31, 'uuu', '77', '77', 'da@gmail.com'),
	(32, 'oooo', '777', '888', 'da@gmail.com'),
	(33, 'yyyy', '666', '666', 'da@gmail.com'),
	(35, 'p', '99', '88', 'da@gmail.com'),
	(37, 'pp', '88', '999', 'da@gmail.com'),
	(38, 'll', '99', '88', 'da@gmail.com'),
	(39, 'ii', '88', '99', 'da@gmail.com'),
	(40, 'Dabid', '88', '99', 'da@gmail.com'),
	(41, 'g', '5', '5', 'da@gmail.com'),
	(42, 'ñ', '99', '99', 'da@gmail.com'),
	(44, 'p', '00', '00', 'da@gmail.com'),
	(46, 'david', '33', '33', 'da@gmail.com'),
	(47, 'oo', '88', '88', 'da@gmail.com'),
	(48, 'carlos', '22', '22', 'da@gmail.com'),
	(50, 'Zalas', '789', '789', 'h@gmail.com'),
	(51, 'Roberto', '999', '999', 'g@gmail.com'),
	(52, 'gg', '23', '23', 'da@gmail.com'),
	(53, 'Romero', '123', '123', 'r@gmail.com'),
	(54, 'Juan', '786', '876', 'l@gmail.com'),
	(55, 'Juan', '786', '876', 'l@gmail.com'),
	(56, 'Juan', '786', '876', 'l@gmail.com'),
	(57, 'Juan', '786', '876', 'l@gmail.com'),
	(58, 'Juan', '786', '876', 'l@gmail.com'),
	(59, 'Juan', '786', '876', 'l@gmail.com'),
	(60, 'Juan', '786', '876', 'l@gmail.com'),
	(61, 'Juan', '786', '876', 'l@gmail.com'),
	(62, 'Juan', '786', '876', 'l@gmail.com'),
	(63, 'Furroo', '98', '987', 'l@gmail.com'),
	(64, 'Furroo', '98', '987', 'l@gmail.com'),
	(65, 'Furroo', '98', '987', 'l@gmail.com'),
	(66, 'Furroo', '98', '987', 'l@gmail.com'),
	(67, 'gg', '88', '88', 'da@gmail.com'),
	(68, 'gg', '88', '88', 'da@gmail.com'),
	(69, 'Salas', '456', '556', 'da@gmail.com'),
	(70, 'Salas', '456', '556', 'da@gmail.com'),
	(71, 'Salas', '456', '556', 'da@gmail.com'),
	(72, 'Salas', '456', '556', 'da@gmail.com'),
	(73, 'Salas', '456', '556', 'da@gmail.com'),
	(74, 'Salas', '456', '556', 'da@gmail.com'),
	(75, 'Salas', '456', '556', 'da@gmail.com'),
	(76, 'Salas', '456', '556', 'da@gmail.com'),
	(77, 'Salas', '456', '556', 'da@gmail.com'),
	(78, 'Salas', '456', '556', 'da@gmail.com'),
	(79, 'Salas', '456', '556', 'da@gmail.com'),
	(80, 'Salas', '456', '556', 'da@gmail.com'),
	(81, 'Salas', '456', '556', 'da@gmail.com'),
	(82, 'Salas', '456', '556', 'da@gmail.com'),
	(83, 'Salas', '456', '556', 'da@gmail.com'),
	(84, 'Salas', '456', '556', 'da@gmail.com'),
	(85, 'Salas', '456', '556', 'da@gmail.com'),
	(86, 'gomez', '8766', '567', 'da@gmail.com'),
	(87, 'gomez', '8766', '567', 'da@gmail.com'),
	(88, 'gomez', '8766', '567', 'da@gmail.com'),
	(89, 'gomez', '8766', '567', 'da@gmail.com'),
	(90, 'gomez', '8766', '567', 'da@gmail.com'),
	(91, 'gomez', '8766', '567', 'da@gmail.com'),
	(92, 'gomez', '8766', '567', 'da@gmail.com'),
	(93, 'gomez', '8766', '567', 'da@gmail.com'),
	(94, 'Eskemy', '9876', '678', 'da@gmail.com'),
	(95, 'Eskemy', '9876', '678', 'da@gmail.com'),
	(96, 'Eskemy', '9876', '678', 'da@gmail.com'),
	(97, 'Eskemy', '9876', '678', 'da@gmail.com'),
	(98, 'Eskemy', '9876', '678', 'da@gmail.com'),
	(99, 'Eskemy', '9876', '678', 'da@gmail.com'),
	(100, 'Eskemy', '9876', '678', 'da@gmail.com'),
	(101, 'hhh', '777', '777', 'da@gmail.com'),
	(102, 'qq', '555', '444', 'da@gmail.com'),
	(103, 'hh', '66', '66', 'dasa@gmail.com'),
	(104, 'uu', '55', '55', 'da@gmail.com'),
	(105, 'Salukis', '788', '778', 'da@gmail.com'),
	(106, 'Romeruki', '998', '777', 'de@gmail.com');
/*!40000 ALTER TABLE `agenteaduanal` ENABLE KEYS */;

-- Volcando estructura para tabla vovo1.colaborador
CREATE TABLE IF NOT EXISTS `colaborador` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) DEFAULT NULL,
  `ApellidoP` varchar(50) DEFAULT NULL,
  `ApellidoM` varchar(50) DEFAULT NULL,
  `Fecha_Nac` varchar(50) DEFAULT NULL,
  `RFC` varchar(50) DEFAULT NULL,
  `Curp` varchar(50) DEFAULT NULL,
  `Direccion` varchar(50) DEFAULT NULL,
  `Celular` varchar(50) DEFAULT NULL,
  `Telefono` varchar(50) DEFAULT NULL,
  `Correo` varchar(50) DEFAULT NULL,
  `Departamento` varchar(50) DEFAULT NULL,
  `Puesto` varchar(50) DEFAULT NULL,
  `Password` varchar(50) DEFAULT 'vovo123',
  `Status` varchar(15) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla vovo1.colaborador: ~0 rows (aproximadamente)
DELETE FROM `colaborador`;
/*!40000 ALTER TABLE `colaborador` DISABLE KEYS */;
INSERT INTO `colaborador` (`ID`, `Nombre`, `ApellidoP`, `ApellidoM`, `Fecha_Nac`, `RFC`, `Curp`, `Direccion`, `Celular`, `Telefono`, `Correo`, `Departamento`, `Puesto`, `Password`, `Status`) VALUES
	(1, 'david', 'salas', 'romero', '12-02-1998', 'dd', 'dd', 'd', '99', '99', 'da@gmail.com', 'recursos humanos', 'Admin', '123', '1'),
	(3, 'Dodo', 'kkk', 'kkk', '999', 'iij', 'jjk', 'jjk', '9999', '9999', 'dsda@gmail.com', 'jjkk', 'Admin', '1234567', '1'),
	(5, 'fff', 'fff', 'gg', 'ggg', 'fff', 'ggg', 'ff', 'fggf', 'fff', 'Fuego@gmail.com', 'fff', 'Admin', '12345', '1'),
	(6, 'hhuu', 'i', 'ii', '2018-02-11', 'ii', 'ii', 'oo', 'j', 'j', 'Komander@gmail.com', 'jj', 'Admin', '123', '1'),
	(9, 'hhuu', 'ooo', 'iiio', '2018-02-17', 'jjj', 'hhh', 'jjj', '9988', '9988', 'ff@gmail.com', 'bbvbb', 'Admin', '1234', '1'),
	(20, 'u', 'u', 'u', '2018-02-17', 'h', 'h', 'h', '98', '899', 'daZa@gmail.com', 'rr', 'Admin', '123', '1'),
	(21, 'l', 'l', 'l', '2018-02-18', 'l', 'l', 'l', 'l', 'l', 'l', 'l', 'Admin', '1234', '1'),
	(22, 'Prueba', 'Prueba', 'Prueba', '2018-02-28', '123PRU', 'QWE123', 'Calle prueba', '9900990099', '1122334455', 'prueba@prueba.com', 'Recursos humanos', 'User', '123', '1'),
	(23, 'jj', '11', 'qq', '1992-01-01', '123', '123', '123', '123', '123', '123@gmail.com', 'Recursos humanos', 'Admin', '123', '1'),
	(24, '1', '1', '1', '1991-11-11', '1', '1', '1', '1', '1', '1', '1', 'Admin', '1', '1'),
	(25, 'Prueba', '11', 'Prueba', '1199-11-11', '11', '11', '11', '11', '11', '11', '11', 'Admin', '111', '1');
/*!40000 ALTER TABLE `colaborador` ENABLE KEYS */;

-- Volcando estructura para tabla vovo1.datosagenciaaduana
CREATE TABLE IF NOT EXISTS `datosagenciaaduana` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NombreAgencia` varchar(50) NOT NULL,
  `Calle` varchar(50) NOT NULL,
  `Nexterio` varchar(50) NOT NULL,
  `Ninterior` varchar(50) NOT NULL,
  `Colonia` varchar(50) NOT NULL,
  `Delegacion` varchar(50) NOT NULL,
  `CP` varchar(50) NOT NULL,
  `PaginaW` varchar(200) NOT NULL,
  `Transporte` varchar(50) DEFAULT NULL,
  `TipoT` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_datosagenciaaduana_agenciaduanal` (`NombreAgencia`),
  CONSTRAINT `FK_datosagenciaaduana_agenciaduanal` FOREIGN KEY (`NombreAgencia`) REFERENCES `agenciaduanal` (`Nombre`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Volcando datos para la tabla vovo1.datosagenciaaduana: ~0 rows (aproximadamente)
DELETE FROM `datosagenciaaduana`;
/*!40000 ALTER TABLE `datosagenciaaduana` DISABLE KEYS */;
/*!40000 ALTER TABLE `datosagenciaaduana` ENABLE KEYS */;

-- Volcando estructura para tabla vovo1.empresa
CREATE TABLE IF NOT EXISTS `empresa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NombreEm` varchar(50) NOT NULL,
  `RepreRFC` varchar(50) NOT NULL,
  `RazonSocial` varchar(50) NOT NULL,
  `RFC` varchar(50) NOT NULL,
  `Estado` varchar(50) NOT NULL,
  `Ciudad` varchar(50) NOT NULL,
  `Pais` varchar(50) NOT NULL,
  `Telefono` varchar(50) NOT NULL,
  `Celular` varchar(50) NOT NULL,
  `Correo` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NombreEm` (`NombreEm`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla vovo1.empresa: ~0 rows (aproximadamente)
DELETE FROM `empresa`;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;

-- Volcando estructura para procedimiento vovo1.insertEmpresa
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertEmpresa`(
	IN `nom` VARCHAR(50),
	IN `repre` VARCHAR(50),
	IN `rasoc` VARCHAR(50),
	IN `rfc` VARCHAR(50),
	IN `edo` VARCHAR(50),
	IN `ciudad` VARCHAR(50),
	IN `pais` VARCHAR(50),
	IN `tel` VARCHAR(50),
	IN `cel` VARCHAR(50),
	IN `correo` VARCHAR(50)
)
BEGIN
INSERT INTO empresa(NombreEm,RepreRFC,RazonSocial,RFC,Estado,Ciudad,Pais,Telefono,Celular,Correo)
VALUES(nom,repre,rasoc,rfc,edo,ciudad,pais,tel,cel,correo);
END//
DELIMITER ;

-- Volcando estructura para tabla vovo1.procesoimportacion
CREATE TABLE IF NOT EXISTS `procesoimportacion` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NombreImport` varchar(50) NOT NULL,
  `NombreCola` varchar(50) NOT NULL,
  `FechaI` date NOT NULL,
  `FechaF` date NOT NULL,
  `NombreEmp` varchar(50) NOT NULL,
  `CostoImp` float NOT NULL,
  `DiasPro` varchar(50) NOT NULL,
  `NombreAgecia` varchar(50) NOT NULL,
  `MontoTrans` float NOT NULL,
  `DireccionAlma` varchar(150) NOT NULL,
  `NombreAgente` varchar(50) NOT NULL,
  `NombreAduana` varchar(50) NOT NULL,
  `Verificacion` varchar(10) NOT NULL,
  `FechaCarga` date NOT NULL,
  `FechaSalida` date NOT NULL,
  `NombreTransporte` varchar(50) NOT NULL,
  `TipoTrans` varchar(50) NOT NULL,
  `MontoCostoTrans` float NOT NULL,
  `TiempoImport` varchar(50) NOT NULL,
  `Inicio` varchar(50) NOT NULL,
  `Cierre` varchar(50) NOT NULL,
  `FechaEntrega` date NOT NULL,
  `NombreRecibio` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla vovo1.procesoimportacion: ~34 rows (aproximadamente)
DELETE FROM `procesoimportacion`;
/*!40000 ALTER TABLE `procesoimportacion` DISABLE KEYS */;
INSERT INTO `procesoimportacion` (`ID`, `NombreImport`, `NombreCola`, `FechaI`, `FechaF`, `NombreEmp`, `CostoImp`, `DiasPro`, `NombreAgecia`, `MontoTrans`, `DireccionAlma`, `NombreAgente`, `NombreAduana`, `Verificacion`, `FechaCarga`, `FechaSalida`, `NombreTransporte`, `TipoTrans`, `MontoCostoTrans`, `TiempoImport`, `Inicio`, `Cierre`, `FechaEntrega`, `NombreRecibio`) VALUES
	(2, 'k', 'k', '2018-01-03', '2018-01-11', 'j', 8, '8', 'h', 8, 'h', 'h', 'h', 'h', '2018-01-03', '2018-01-03', 's', 's', 0, 's', 's', 's', '2018-01-31', 's'),
	(3, 'ffkkk', 'ffkkk', '2018-01-01', '2018-01-02', 'ff', 4, '4', 'ff', 4, 'r', 'r', 'r', 'r', '2018-01-03', '2018-01-04', 'f', 'f', 0, 'f', 'f', 'f', '2018-01-05', 'f'),
	(4, 'Zalas', 'ff', '0000-00-00', '0000-00-00', 'ff', 4, '4', 'ff', 4, 'r', 'r', 'r', 'r', '0000-00-00', '0000-00-00', 'f', 'ZalasZ', 0, 'f', 'f', 'f', '0000-00-00', 'f'),
	(5, 'salas', 'Zalukis', '0000-00-00', '0000-00-00', 'Zalukis', 8, '8', 'Zalukis', 8, 'Zalukis', 'Zalukis', 'Zalukis', 'Zalukis', '0000-00-00', '0000-00-00', 'Zalukis', 'Zalukis', 0, 'Zalukis', 'Zalukis', 'Zalukis', '0000-00-00', 'Zalukis'),
	(7, 'salas', 'zalas', '0000-00-00', '0000-00-00', '', 0, '', '', 900, '', '', '', '', '0000-00-00', '0000-00-00', '', '', 0, '', '', '', '0000-00-00', 'xxx'),
	(11, 'Zalukis 234', '', '2018-01-20', '2018-01-14', '', 0, '', '', 0, '', '', '', '', '2018-01-06', '2018-01-27', '', '', 0, '', '', '', '2018-01-17', 'dd'),
	(14, 'Zalas El perro', '', '2018-01-26', '2018-01-25', '', 500, '', '', 44464, '', '', '', '', '2018-01-12', '2018-01-27', '', '', 0, '', '', '', '2018-01-28', ''),
	(17, 'vvvv', '', '0000-00-00', '0000-00-00', '', 0, '', '', 50004, '', '', '', '', '0000-00-00', '0000-00-00', '', '', 0, '', '', '', '0000-00-00', ''),
	(18, 'ff', '', '2018-01-24', '2018-01-21', '', 0, '', '', 5, '', '', '', '', '2018-01-14', '2018-01-09', '', '', 0, '', '', '', '2018-01-25', ''),
	(19, 'ff', '', '2018-01-26', '2018-01-21', '', 66, '', '', 66, '', '', '', '', '2018-01-07', '2018-01-07', '', '', 6788, '', '', '', '2018-01-21', ''),
	(22, 'g', '', '0000-00-00', '0000-00-00', '', 0, '', '', 444, '', '', '', '', '0000-00-00', '0000-00-00', '', '', 0, '', '', '', '0000-00-00', ''),
	(24, 'veveve', '', '2018-01-16', '2018-01-27', '', 0, '', '', 1111, '', '', '', '', '2018-01-12', '2018-01-19', '', '', 0, '', '', '', '2018-01-28', ''),
	(25, 'dd', '', '2018-01-04', '2018-01-14', '', 0, '', '', 200, '', '', '', '', '2018-01-08', '2018-01-05', '', '', 0, '', '', '', '2018-01-27', ''),
	(26, 'Salas<', '', '2018-01-06', '2018-01-19', '', 0, '', '', 2000, '', '', '', '', '2018-01-12', '2018-01-27', '', '', 0, '', '', '', '2018-01-30', ''),
	(28, 'uuu', '', '2018-01-13', '2018-01-11', '', 0, '', '', 99987, '', '', '', '', '2018-01-25', '2018-01-05', '', '', 0, '', '', '', '2018-01-25', ''),
	(29, 'uuu', '', '2018-01-13', '2018-01-11', '', 0, '', '', 99987, '', '', '', '', '2018-01-25', '2018-01-05', '', '', 0, '', '', '', '2018-01-25', ''),
	(31, 'uuu', '', '2018-01-13', '2018-01-11', '', 0, '', '', 99987, '', '', '', '', '2018-01-25', '2018-01-05', '', '', 0, '', '', '', '2018-01-25', ''),
	(32, 'uuu', '', '2018-01-13', '2018-01-11', '', 0, '', '', 99987, '', '', '', '', '2018-01-25', '2018-01-05', '', '', 0, '', '', '', '2018-01-25', ''),
	(33, 'uuu', '', '2018-01-13', '2018-01-11', '', 0, '', '', 99987, '', '', '', '', '2018-01-25', '2018-01-05', '', '', 0, '', '', '', '2018-01-25', ''),
	(34, 'uuu', '', '2018-01-13', '2018-01-11', '', 0, '', '', 99987, '', '', '', '', '2018-01-25', '2018-01-05', '', '', 0, '', '', '', '2018-01-25', ''),
	(36, 'uuuyy', '', '2018-01-04', '2018-01-13', '', 0, '', '', 888888, '', '', '', '', '2018-01-03', '2018-01-25', '', '', 0, '', '', '', '2018-01-12', ''),
	(37, 'ZaZa', '', '2018-01-19', '2018-01-16', '', 5, '', '', 666, '', '', '', '', '2018-01-08', '2018-01-02', '', '', 0, '', '', '', '2018-01-21', ''),
	(38, 'jjjj', '', '2018-02-28', '2018-01-31', '', 0, '', '', 9000, '', '', '', '', '2018-02-28', '2018-01-04', '', '', 0, '', '', '', '2018-04-30', ''),
	(39, 'Zalazuu', '', '2018-01-31', '2018-01-31', '', 0, '', '', 0, '', '', '', '', '2018-01-31', '2018-01-31', '', '', 0, '', '', '', '2018-01-31', ''),
	(40, 'jjj', '', '2018-01-14', '2018-01-17', '', 0, '', '', 9000, '', '', '', '', '2018-01-10', '2018-01-09', '', '', 0, '', '', '', '2018-01-27', ''),
	(43, 'rrrr', '', '2018-01-14', '2018-01-27', '', 0, '', '', 9000, '', '', '', '', '2018-01-13', '2018-01-27', '', '', 0, '', '', '', '2018-01-16', ''),
	(46, 'fff', '', '2018-02-24', '2018-02-18', '', 0, '', '', 13000, '', '', '', '', '2018-02-13', '2018-02-25', '', '', 0, '', '', '', '2018-02-25', ''),
	(47, 'ññ', '', '2018-02-14', '2018-02-08', '', 0, '', '', 234, '', '', '', '', '2018-02-21', '2018-02-24', '', '', 0, '', '', '', '2018-02-24', ''),
	(48, 'Eskemy', '', '2018-02-15', '2018-02-17', '', 0, '', '', 1500, '', '', '', '', '2018-02-21', '2018-02-16', '', '', 0, '', '', '', '2018-02-18', ''),
	(49, 'jjj', '', '2018-02-10', '2018-02-15', '', 0, '', '', 2500, '', '', '', '', '2018-02-18', '2018-02-02', '', '', 0, '', '', '', '2018-02-23', ''),
	(50, 'yyy', '', '2018-02-14', '2018-02-17', '', 0, '', '', 0, '', '', '', '', '2018-02-09', '2018-02-10', '', '', 0, '', '', '', '2018-02-16', ''),
	(51, 'Aviones', 'Joshua', '2018-02-24', '2018-02-20', 'Dia', 399, '2', 'Hindas', 444, 'Calle de las esperanzas', 'John', 'Tizimin', 'No', '2018-02-02', '2018-02-07', 'Gomez', 'Avión', 300, '2', 'Ayer', 'Hoy', '2018-02-03', 'Zuriel'),
	(52, 'yy', 'yy', '2018-02-26', '2018-02-25', '', 0, '', '', 0, '', '', '', '', '0000-00-00', '2018-02-17', '', '', 0, '', '', '', '2018-02-28', '');
/*!40000 ALTER TABLE `procesoimportacion` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
